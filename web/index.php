<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Test App</title>

    <!--    jQuery Popup Authorization-->
    <link rel="stylesheet" href="css/normalize.css">
    <link rel='stylesheet prefetch'
          href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,700italic,400italic'>
    <link rel='stylesheet prefetch'
          href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css'>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div>

    <!--This div is container for authorization-->
    <div class="container">
        <a id="modal_trigger" href="#modal" class="btn">Authorization</a>

        <div id="modal" class="popupContainer" style="display:none;">
            <header class="popupHeader">
                <span class="header_title">Authorize</span>
                <span class="modal_close"><i class="fa fa-times"></i></span>
            </header>

            <section class="popupBody">
                <!-- Login + SignUp + Logout-->
                <div class="social_login">
                    <div class="centeredText">
                        <span>Use your Email address</span>
                    </div>

                    <div class="action_btns">
                        <div class="one_half" id="loginBtn"><a href="#" id="login_form" class="btn">Login</a></div>
                        <div style="display: none;" id="logoutBtn" class="one_half"><a href="#" id="logout_form"
                                                                                       class="btn">Logout</a></div>
                        <div class="one_half last"><a href="#" id="register_form" class="btn">Sign up</a></div>
                    </div>
                </div>

                <!-- Login form & Restore password button -->
                <div class="user_login">
                    <form id="1">
                        <label>Email</label>
                        <input type="text" id="logEmail"/>
                        <br/>

                        <label class="hide_for_forgot_password">Password</label>
                        <input type="password" id="logPassword" class="hide_for_forgot_password"/>
                        <br/>

                        <div class="action_btns">
                            <div class="one_half"><a href="#" class="btn back_btn"><i
                                        class="fa fa-angle-double-left"></i> Back</a></div>
                            <div class="one_half last"><a href="#" id="logSubmit" class="btn btn_red">Login</a>
                            </div>
                            <div style="display: none" id="restoreButton" class="one_half last"><a href="#"
                                                                                                   id="restoreSubmit"
                                                                                                   class="btn btn_red">Restore</a>
                            </div>
                        </div>
                    </form>

                    <a href="#" id="forgotPassword" class="forgot_password">Forgot password?</a>
                </div>

                <!-- Register Form -->
                <div class="user_register">
                    <form>
                        <label>Email Address</label>
                        <input type="email" id="regEmail"/>
                        <br/>

                        <div class="action_btns">
                            <div class="one_half"><a href="#" class="btn back_btn"><i
                                        class="fa fa-angle-double-left"></i> Back</a></div>
                            <div class="one_half last"><a href="#" id="regSubmit" class="btn btn_red">Register</a>
                            </div>
                        </div>
                    </form>
                </div>

            </section>
        </div>
    </div>

    <!--This panel to upload and display pictures -->
    <div class="panel" <?= isset($_COOKIE['session_token']) ? '' : 'style="display: none;"' ?>>
        <div id="progressBar" style="display: none"><img  style="height: 100px; top:0;left: 0; position: absolute;" src="images/1414255352_loading-01.gif" alt="upload... wait!"></div>
        <form name=loadavatar id=loadavatar enctype=multipart/form-data action=../download.php method=post ><input
                type=hidden name=MAX_FILE_SIZE value=16777216>
            <input multiple="true" id=avatarfile name=file[] type=file>
            <input value="Загрузить" onclick="upload()" type="submit">
        </form>

        <p><div class="photos"><? include 'template/photoPanel.php' ?></div></p>
    </div>

    <!--   Needed to be after authorization container - jQuery Popup Authorization-->
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src='http://malsup.github.io/min/jquery.form.min.js'></script>
    <script src='http://andwecode.com/wp-content/uploads/2015/10/jquery.leanModal.min_.js'></script>
    <script src="js/index.js"></script>

</div>
</body>
</html>