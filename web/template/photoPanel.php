<?php

/**
 * Template for displaying images
 */
if (isset($_COOKIE['session_token'])) {
    include($_SERVER['DOCUMENT_ROOT'] . '/src/Controller/ImagesController.php');
    $image = new ImagesController();
    $json = $image->load();
    if ($json) {
        $photos = '';
        foreach (json_decode($json) as $item) {
            $photos .= "<a href='" . $item->path . "' target='_blank'><img style='height: 100px; padding:5px' src='" . $item->preview_path . "' alt='" . $item->name . "'</a>";
        }
        echo $photos;
    }
}