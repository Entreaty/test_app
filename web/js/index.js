$("#modal_trigger").leanModal({
    top: 100,
    overlay: 0.6,
    closeButton: ".modal_close"
});

$(document).ready(function () {

    // Sets default style for buttons Login&Logout
    if (getCookie('session_token')) {
        $('#logoutBtn').css("display", "block");
        $('#loginBtn').css("display", "none");
    } else {
        $('#logoutBtn').css("display", "none");
        $('#loginBtn').css("display", "block");
    }
});


$(function () {

    // Calling Login Form
    $("#login_form").click(function () {
        $(".social_login").hide();
        $(".user_login").show();
        return false;
    });

    // Calling Register Form
    $("#register_form").click(function () {
        $(".social_login").hide();
        $(".user_register").show();
        $(".header_title").text('Register');
        return false;
    });

    // Going back to Both Forms
    $(".back_btn").click(function () {
        $(".user_login").hide();
        $(".user_register").hide();
        $(".social_login").show();
        $(".header_title").text('Login');
        return false;
    });

    // Register user
    $('#regSubmit').on('click', function () {
        email = $('#regEmail').val();
        $.ajax({
            type: "POST",
            url: "/src/Router/Router.php",
            data: {"email": email, "controller": "AuthController", "method": "signUp"},
            statusCode: {
                400: function () {
                    alert('Please fill correctly Email address!');
                },
                403: function () {
                    alert('This Email address already exist');
                },
                201: function () {
                    alert('Success! Check your Email address');
                    $(".user_login").hide();
                    $(".user_register").hide();
                    $(".social_login").show();
                    $(".header_title").text('Login');
                }
            }
        });
    });

    // Login user
    $('#logSubmit').on('click', function () {
        email = $('#logEmail').val();
        password = $('#logPassword').val();
        $.ajax({
            type: "POST",
            url: "/src/Router/Router.php",
            data: {"email": email, "password": password, "controller": "AuthController", "method": "signIn"},
            statusCode: {
                400: function () {
                    alert('Please fill correctly Email address and Password!');
                },
                401: function () {
                    alert('Login or password is wrong!');
                },
                201: function () {
                    $('#modal').toggle();
                    $('#lean_overlay').toggle();
                    $('#logoutBtn').toggle();
                    $('#loginBtn').toggle();
                    // $('.panel').toggle();
                    window.location.reload();
                }
            }
        });
    });

    // Restore password
    $('#forgotPassword').on('click', function () {
        $('.hide_for_forgot_password').toggle();
        $('#logSubmit').toggle();
        $('#restoreButton').toggle();
        email = $('#logEmail').val();
        console.log(email);
        $('#restoreSubmit').on('click', function () {
            $.ajax({
                type: "POST",
                url: "/src/Router/Router.php",
                data: {"email": email, "controller": "AuthController", "method": "restorePassword"},
                statusCode: {
                    400: function () {
                        alert('Please fill correctly Email!');
                    },
                    404: function () {
                        alert('This Email address is not registered');
                    },
                    200: function () {
                        alert('Success! Check your Email address');
                    }
                }
            });
        })
    });

    // Logout
    $('#logoutBtn').on('click', function () {
        $('#logoutBtn').toggle();
        $('#loginBtn').toggle();
        key = getCookie('session_token');
        console.log(key);
        $.ajax({
            type: "POST",
            url: "/src/Router/Router.php",
            data: {"key": key, "controller": "AuthController", "method": "logout"},
            statusCode: {
                400: function () {
                    alert('Please fill correctly Email!');
                },
                404: function () {
                    alert('This Email address does not registered');
                },
                204: function () {
                    console.log('logout success');
                    $('.panel').toggle();
                }
            }
        });

    });
});

//Get cookie from session
function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}


function upload() {
    $('#progressBar').toggle();
    // Send images
    $("#loadavatar").ajaxForm({
        url: "/src/Router/Router.php", type: 'post',
        data: {"controller": "ImagesController", "method": "upload"},
        success: function () {
            // Display image
            $.ajax({
                url: "/web/template/photoPanel.php",
                type: "POST",
                success: function (msg) {
                    $('.photos').html('');
                    $('#progressBar').toggle();
                    $('.photos').append(msg);
                }
            });
        }
    });
    
}