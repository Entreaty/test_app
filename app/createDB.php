<?php


require_once($_SERVER['DOCUMENT_ROOT'] . '/src/Config.php');

/**
 * Create table at localhost MySQL
 */

// Create connection
$conn = new mysqli('localhost', 'root', '');
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
// Drop exist DB
$conn->query("DROP DATABASE IF EXISTS " . DB_LOC_TABLE_NAME);

// Create database
$sql = "CREATE DATABASE " . DB_LOC_TABLE_NAME;
if ($conn->query($sql) === TRUE) {
    echo "Database created successfully";
  
    /**
     * Create table tokens
     */
    $sql1 = "CREATE TABLE IF NOT EXISTS `" . DB_LOC_TABLE_NAME . "`.`tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_UNIQUE` (`key`),
  KEY `users_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
    
    $sql1_a = "ALTER TABLE `" . DB_LOC_TABLE_NAME . "`.`tokens` 
ADD INDEX `key_idx` (`key`(30) ASC)  COMMENT '',
ADD INDEX `deletedAt_idx` (`deleted_at` ASC)  COMMENT '';
";
    /**
     * Create table users
     */
    $sql2 = "CREATE TABLE IF NOT EXISTS `" . DB_LOC_TABLE_NAME . "`.`users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `password` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;";
    $sql2_a = "ALTER TABLE `" . DB_LOC_TABLE_NAME . "`.`users` 
ADD INDEX `email_idx` (`email`(15) ASC)  COMMENT '',
ADD INDEX `password_idx` (`password`(33) ASC)  COMMENT '',
ADD INDEX `deletedt_idx` (`deleted_at` ASC)  COMMENT '';
";
    /**
     * Create table downloads
     */
    $sql3 = "CREATE TABLE `" . DB_LOC_TABLE_NAME . "`.`download` (
  `id` INT NULL AUTO_INCREMENT COMMENT '',
  `name` VARCHAR(255) NULL COMMENT '',
  `path` VARCHAR(255) NULL COMMENT '',
  `preview_path` VARCHAR(255) NULL COMMENT '',
  `user_id` INT NULL COMMENT '',
  `created_at` DATETIME NULL COMMENT '',
  `deleted_at` DATETIME NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  UNIQUE INDEX `id_UNIQUE` (`id` ASC)  COMMENT '',
  INDEX `id_idx` (`user_id` ASC)  COMMENT '',
  CONSTRAINT `user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `" . DB_LOC_TABLE_NAME . "`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);";
    $sql3_a = "ALTER TABLE `" . DB_LOC_TABLE_NAME . "`.`download` 
DROP INDEX `id_idx` ,
ADD INDEX `userId_idx` (`user_id` ASC)  COMMENT '',
ADD INDEX `name_idx` (`name`(10) ASC)  COMMENT '',
ADD INDEX `deleted_idx` (`deleted_at` ASC)  COMMENT '';";
    
    if ($conn->query($sql1) && $conn->query($sql1_a) && $conn->query($sql2) && $conn->query($sql2_a)
        && $conn->query($sql3) && $conn->query($sql3_a) === TRUE
    ) {
        echo "\n Tables created successfully";
    } else {
        echo "\n Error creating Tables: " . $conn->error;
    }
} else {
    echo "Error creating database: " . $conn->error;
}

$conn->close();

   