<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/src/Repository/DBRepository.php');

/**
 * Class UserRepository
 */
class UserRepository extends DBRepository
{
    /**
     * @param $email
     * @return object|stdClass
     */
    public function getUserByEmail($email)
    {
        $query = $this->db()->query("SELECT *  FROM `users` WHERE `email` = '" . $email . "' AND `deleted_at` IS NULL");
        return $query->fetch_object();
    }

    /**
     * @param $email
     * @param $password
     */
    public function createUser($email, $password)
    {
        $currentDate = (new DateTime('now'))->format('Y-m-d H:i:s');
        $this->db()->query("INSERT INTO `users` (`id`, `email`, `password`, `created_at`, `updated_at`, `deleted_at`) VALUES (NULL, '" . $email . "', '" . $password . "', '" . $currentDate . "', NULL, NULL);");
    }

    /**
     * @param $userId
     * @param $password
     */
    public function updateUser($userId, $password)
    {
        $this->db()->query("UPDATE `users` SET `password` = '".$password."' WHERE `users`.`id` = ".$userId);
    }

    /**
     * @param $email
     * @param $password
     * @return object|stdClass
     */
    public function checkPassword($email, $password)
    {
        $query = $this->db()->query("SELECT * FROM `users` as u WHERE u.`email` = '" . $email . "' AND u.`password` = '" . $password . "' AND u.`deleted_at` IS NULL");
        return $query->fetch_object();
    }

    /**
     * @param $key
     * @param $userId
     */
    public function createSessionToken($key, $userId)
    {
        $currentDate = (new DateTime('now'))->format('Y-m-d H:i:s');
        $expireData = (new DateTime('+2 weeks'))->format('Y-m-d H:i:s');
        $this->db()->query("INSERT INTO `tokens` (`id`, `key`, `user_id`, `created_at`, `expires_at`, `deleted_at`) VALUES (NULL, '" . $key . "', '" . $userId . "', '" . $currentDate . "', '" . $expireData . "', NULL)");
    }

    /**
     * @param $key
     * @return object|stdClass
     */
    public function getSessionToken($key){
        $query = $this->db()->query("SELECT * FROM `tokens` WHERE `key` = '".$key."'");
        return $query->fetch_object();
    }

    /**
     * @param $tokenId
     */
    public function deleteSessionToken($tokenId){
        $currentDate = (new DateTime('now'))->format('Y-m-d H:i:s');
        $this->db()->query("UPDATE `tokens` SET `deleted_at` = '".$currentDate."' WHERE `tokens`.`id` = ".$tokenId);
        
    }

    /**
     * @return bool|mysqli_result
     */
    public function getUserByToken(){
        return $this->db()->query("SELECT * FROM `tokens` WHERE `key` = '".$_COOKIE['session_token']."' AND `deleted_at` IS NULL");
    }
    
    
}