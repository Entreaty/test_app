<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/src/Repository/DBRepository.php');

/**
 * Class ImagesRepository
 */
class ImagesRepository extends DBRepository
{
    /**
     * @param $name
     * @param $userId
     * @return bool|mysqli_result
     */
    public function findImage($name, $userId){
        return $this->db()->query("SELECT * FROM `download` WHERE `name` = '".$name."' AND `user_id` = ".$userId);
    }

    /**
     * @param $name
     * @param $path
     * @param $previewPath
     * @param $userId
     */
    public function saveImageToDB($name,$path,$previewPath,$userId){
        $currentDate = (new DateTime('now'))->format('Y-m-d H:i:s');
        $this->db()->query("INSERT INTO `download` (`id`, `name`, `path`, `preview_path`, `user_id`, `created_at`, `deleted_at`) VALUES (NULL, '".$name."', '".$path."', '".$previewPath."', '".$userId."', '".$currentDate."', NULL);");
    }

    /**
     * @param $userId
     * @return bool|mysqli_result
     */
    public function getImages($userId){
        return $this->db()->query("SELECT * FROM `download` WHERE `user_id` = ".$userId);
    }
}
