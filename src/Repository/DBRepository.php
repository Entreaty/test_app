<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/src/Service/Database.php');
/**
 * Class DBRepository
 */
class DBRepository
{
    /**
     * @var mysqli
     */
    private $mysqli;

    /**
     * DBRepository constructor.
     */
    function __construct()
    {
        $db = Database::getInstance();
        $this->mysqli = $db->getConnection();
    }

    /**
     * @return mysqli
     */
    public function db(){
        return $this->mysqli;
    }
}