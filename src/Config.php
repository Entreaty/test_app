<?php

/**
 * List of constants
 */

// MySQL params on localhost
const DB_LOC_SERVER_NAME = "localhost";
const DB_LOC_USER_NAME = "root";
const DB_LOC_PASSWORD = "";
const DB_LOC_TABLE_NAME = "test_app";

// MySQL params on hosting
const DB_SERVER_NAME = "localhost";
const DB_USER_NAME = "u603220317_us";
const DB_PASSWORD = "123456";
const DB_TABLE_NAME = "u603220317_db";

// Require for generate password
const SALT = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

// Folder for uploading
const UPLOAD_DIR = 'web/uploads';

// Preview image params
const PREVIEW_WIDTH = 100;
const PREVIEW_HEIGHT = 100;
const PREVIEW_COLOR = 0xF5F3F0;
