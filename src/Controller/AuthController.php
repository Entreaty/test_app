<?php

require_once('../Service/SendEmail.php');
require_once('../Repository/UserRepository.php');

/**
 * Class AuthController
 */
class AuthController
{
    /**
     * Registration
     */
    public function signUp()
    {
        if (isset($_POST['email']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $email = $_POST['email'];
            $userRepository = new UserRepository();
            $user = $userRepository->getUserByEmail($email);
            if (isset($user)) {
                return http_response_code(403);
            }
            $randomPassword = $this->generateShortPassword(5);
            $password = $this->generatePassword($randomPassword);
            $userRepository->createUser($email, $password);
            (new SendEmail())->send($email, $randomPassword);
            return http_response_code(201);
        } else {
            return http_response_code(400);
        }
    }

    /**
     * Generate password for DB
     * @param $string
     * @return string
     */
    private function generatePassword($string){
        return md5($string . SALT);
    }
    
    /**
     * Generate short password
     * @param int $length
     * @return string
     */
    public function generateShortPassword($length = 5)
    {
        $chars = '01234567890123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $count = mb_strlen($chars);
        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }
        return $result;
    }

    /**
     * Login
     */
    public function signIn()
    {
        if (isset($_POST['email']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) && isset($_POST['password']) && ($_POST['password'] !== '')) {
            $email = $_POST['email'];
            $password = $this->generatePassword($_POST['password']);
            $userRepository = new UserRepository();
            $result = $userRepository->checkPassword($email, $password);
            if (!isset($result)) {
                return http_response_code(401);
            }
            $key = $this->generateKey();
            $userRepository->createSessionToken($key, $result->id);

            //set Token key for session with 2 weeks life
            setcookie("session_token", $key, (new DateTime('2 weeks'))->getTimestamp(), '/');
            return http_response_code(201);
        } else {
            return http_response_code(400);
        }
    }

    /**
     * Generate Token key
     * @return string
     */
    public function generateKey()
    {
        return hash('ripemd160', (new DateTime('now'))->getTimestamp());
    }

    /**
     * Restore the password
     */
    public function restorePassword()
    {
        if (isset($_POST['email']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $email = $_POST['email'];
            $userRepository = new UserRepository();
            $user = $userRepository->getUserByEmail($email);
            if (!isset($result)) {
                return http_response_code(404);
            }
            $randomPassword = $this->generateShortPassword(5);
            $password = $this->generatePassword($randomPassword);
            $userRepository->updateUser($user->id, $password);
            (new SendEmail())->send($email, $randomPassword);
            return http_response_code(200);
        } else {
            return http_response_code(400);
        }

    }

    /**
     * Logout
     */
    public function logout()
    {
        if (isset($_POST['key'])) {
            $key = $_POST['key'];
            $userRepository = new UserRepository();
            $result = $userRepository->getSessionToken($key);
            if (!isset($result)) {
                return http_response_code(401);
            }
            $userRepository->deleteSessionToken($result->id);

            setcookie("session_token", "", time() - 3600, '/');
            return http_response_code(204);
        } else {
            return http_response_code(400);
        }
    }

}