<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/src/Repository/UserRepository.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/src/Repository/ImagesRepository.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/src/Config.php');

/**
 * Class ImagesController
 */
class ImagesController
{
    /**
     * Upload images
     */
    public function upload()
    {
        $uploads_dir = $_SERVER['DOCUMENT_ROOT'] . '/' . UPLOAD_DIR;
        if (file_exists($uploads_dir)) {
        } else {
            mkdir($uploads_dir, 0777, true);
        }
        if ($_FILES["file"]["name"][0] !== '') {
            foreach ($_FILES["file"]["error"] as $key => $error) {
                //  Check and upload images
                if ($error == UPLOAD_ERR_OK && exif_imagetype($_FILES["file"]["tmp_name"][$key])) {
                    $tmp_name = $_FILES["file"]["tmp_name"][$key];
                    $this->saveImage($key, $tmp_name, $uploads_dir);
                }
            }
        }
        return http_response_code(201);
    }

    /**
     * Save Image & preview image to server and DataBase
     * @param $key
     * @param $tmp_name
     * @param $uploads_dir
     */
    private function saveImage($key, $tmp_name, $uploads_dir)
    {
        list($name, $type) = explode('.', $_FILES["file"]["name"][$key]);
        $imageName = md5($name) . '.' . $type;
        $path = 'uploads' . '/' . $imageName;
        $pathPreview = 'uploads' . '/preview_' . $imageName;
        $imagePath = $uploads_dir . '/' . $imageName;
        move_uploaded_file($tmp_name, $imagePath);

        $this->createPreviewImage($imagePath, $imageName, $type, $uploads_dir);

        // Add Image info to DataBase
        $userRepository = new UserRepository();
        $imagesRepository = new ImagesRepository();
        $result = $userRepository->getUserByToken()->fetch_object();
        // Check duplicate images
        $image = $imagesRepository->findImage($name, $result->user_id);
        if ($image && !$image->fetch_object())
            $imagesRepository->saveImageToDB($name, $path, $pathPreview, $result->user_id);
    }

    /**
     * Create preview image
     *
     * @param $imagePath
     * @param $imageName
     * @param $type
     * @param $uploads_dir
     */
    public function createPreviewImage($imagePath, $imageName, $type, $uploads_dir)
    {

        $size = getimagesize($imagePath);           // get image width/height
        $extensions = array('jpeg', 'gif', 'png');  // allowed extension
        if (!in_array($type, $extensions)) {$type = 'jpeg';}  // change another extension
        $icfunc = "imagecreatefrom" . $type;        // function definition
        $x_ratio = PREVIEW_WIDTH / $size[0];        // proportion of width
        $y_ratio = PREVIEW_HEIGHT / $size[1];       // proportion of height
        $ratio = min($x_ratio, $y_ratio);
        $use_x_ratio = ($x_ratio == $ratio);        // ratio of width to height
        $new_width = $use_x_ratio ? PREVIEW_WIDTH : floor($size[0] * $ratio);       // preview width
        $new_height = !$use_x_ratio ? PREVIEW_HEIGHT : floor($size[1] * $ratio);    // preview height
        $new_left = $use_x_ratio ? 0 : floor((PREVIEW_WIDTH - $new_width) / 2);     // difference between the latitudes
        $new_top = !$use_x_ratio ? 0 : floor((PREVIEW_HEIGHT - $new_height) / 2);   // difference between the heights
        $img = imagecreatetruecolor(PREVIEW_WIDTH, PREVIEW_HEIGHT);                 // create blank preview
        imagefill($img, 0, 0, PREVIEW_COLOR);                                       // add color
        $photo = $icfunc($imagePath);                                               // load image
        imagecopyresampled($img, $photo, $new_left, $new_top, 0, 0, $new_width, $new_height, $size[0], $size[1]); // create preview
        imagejpeg($img, $uploads_dir . '/preview_' . $imageName);                   // save preview

        // Clear the memory
        imagedestroy($img);
        $img = null;
        imagedestroy($photo);
        $photo = null;
    }

    /**
     * @return string
     */
    public function load()
    {
        if (isset($_COOKIE['session_token'])) {
            $userRepository = new UserRepository();
            $imagesRepository = new ImagesRepository();
            $user = $userRepository->getUserByToken()->fetch_object();
            if ($user) {
                $images = $imagesRepository->getImages($user->user_id);
                $photos = [];
                while ($obj = $images->fetch_object()) {
                    $photo{'id'} = $obj->id;
                    $photo{'name'} = $obj->name;
                    $photo{'path'} = $obj->path;
                    $photo{'preview_path'} = $obj->preview_path;
                    array_push($photos, $photo);
                }
                return json_encode($photos);
            }
        }
    }
}