<?php

/**
 * The entry point for the web.
 * Find Class and execute required method.
 */
$method = $_POST['method'];
$controller = $_POST['controller'];

require_once('../Controller/' . $controller . '.php');
$obj = new $controller();
$obj->$method();

