<?php

/**
 * Class SendEmail
 */
class SendEmail
{
    /**
     * @param $email
     * @param $text
     * @param string $subject
     */
    function send($email, $text, $subject = "Don't answer to this mail")
    {
        mail($email, $subject, $text);
    }

}

