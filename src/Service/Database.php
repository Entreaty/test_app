<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/src/Config.php');

/**
 * Class Database
 * only one connection allowed
 */
class Database {
    private $conn;
    private static $instance;
    private $host;
    private $user;
    private $pass;
    private $data;

    /**
     * @return Database
     */
    public static function getInstance() {
        if(!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Database constructor.
     */
    private function __construct() {
        $this->determineHost();
        $this->conn = new mysqli($this->host, $this->user,
            $this->pass, $this->data);

        if(mysqli_connect_error()) {
            trigger_error("Failed to conencto to MySQL: " . mysqli_connect_error(),
                E_USER_ERROR);
        }
    }

    /**
     * Protect from creation through cloning
     */
    private function __clone() { }

    /**
     * Get mysqli connection
     * @return mysqli
     */
    public function getConnection() {
        return $this->conn;
    }

    /**
     * Defining host
     */
    private function determineHost(){
        if ($_SERVER['SERVER_NAME'] == 'freee.16mb.com') {
            $this->host = DB_SERVER_NAME;
            $this->user = DB_USER_NAME;
            $this->pass = DB_PASSWORD;
            $this->data = DB_TABLE_NAME;
        } else {
            $this->host = DB_LOC_SERVER_NAME;
            $this->user = DB_LOC_USER_NAME;
            $this->pass = DB_LOC_PASSWORD;
            $this->data = DB_LOC_TABLE_NAME;
        }
    }
}